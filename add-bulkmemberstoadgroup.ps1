# Date: 11/18/2020
# Author: Sunny Kahlon
#
# This script will bulk add users from members.csv to AD group specified in $group. 
# members.csv file will need to have header Username in column A1

$group = 'group_name_here'
$users = Import-CSV -Path $PSScriptRoot\members.csv

foreach ($user in $users) {
    Add-ADGroupMember -Identity $group -Members $users.Username
}
