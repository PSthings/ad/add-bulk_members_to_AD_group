Bulk adds users defined in **`members.csv`** to AD group defined in `$group` variable.<br>
Replace **`group_name_here`** placeholder with the correct AD group name

Be sure to `Import-Module ActiveDirectory` if not already imported in session
